using System;


class Program
{
    static Precalculo[] dr = new Precalculo[3];
    static int DatosdeCalculos = 0;

    static void Main(string[] args)
    {

        float Monto, Tasa;
        int Plazo;
        Console.Write("Monto del Prestamo en RD$: ");
        float.TryParse(Console.ReadLine(), out Monto);
        Console.Write("Tasa de Porcentaje Anual: ");
        float.TryParse(Console.ReadLine(), out Tasa);
        Console.Write("Plazo(en meses): ");
        int.TryParse(Console.ReadLine(), out Plazo);

        dr[DatosdeCalculos] = new Precalculo(Monto, Tasa, Plazo);
    }

}


public class Precalculo
{

    static Precalculo[] dr = new Precalculo[1];

    private float _monto;
    private float _tasa;
    private int _plazo;

    public Precalculo(float monto, float tasa, int plazo)
    {
        double InteresMensual, Cuota, NTasa, cuo1, cuo2, cuo3, Balance;

        if (monto <= 0)
        {
            Console.WriteLine("Por Favor Inserte un Número Valido");
            Console.WriteLine();
        }
        else
        {
            Console.WriteLine();
        }
        this._monto = monto;
        this._tasa = tasa;
        this._plazo = plazo;

        DateTime thisDay = DateTime.Today;
        String format = "dd-MMM-yyyy";

        var nuevaFecha = DateTime.Today;

        Balance = this._monto;

        InteresMensual = (tasa / 100) / 12;

        NTasa = tasa / 12;

        cuo1 = (1 + NTasa / 100);
        cuo2 = Math.Pow(cuo1, plazo);
        cuo3 = cuo2 * (NTasa / 100) / (cuo2 - 1);
        Cuota = monto * cuo3;


        double Endeudamiento = (monto - Cuota) * 0.35;

        var Endeudamiento2 = Endeudamiento.ToString("F2");
        Console.WriteLine("Capacidad de Endeudamiento " + Endeudamiento2 + "%");

        Console.Write("Pago \t");
        Console.Write("Fecha de Pago \t\t");
        Console.Write("Cuota \t\t");
        Console.Write("Capital \t\t");
        Console.Write("Interes \t\t");
        Console.Write("Balance \t");
        Console.WriteLine();

        Console.WriteLine();
        double Interes, Capital;
        for (int i = 0; i < plazo; i++)
        {
            Interes = InteresMensual * Balance;
            Capital = Cuota - Interes;
            Balance = Balance - Capital;

            var Cuota3 = Cuota.ToString("F2");
            var Interes2 = Interes.ToString("F2");
            var Capital3 = Capital.ToString("F2");
            var Balance3 = Balance.ToString("F2");

            Console.WriteLine();

            Console.Write("{0}\t", i);
            Console.Write("{0}\t\t", nuevaFecha.ToString(format));
            Console.Write("{0}\t\t", Cuota3);
            Console.Write("{0}\t\t\t", Capital3);
            Console.Write("{0}\t\t\t", Interes2);
            Console.Write("{0}\t", Balance3);
            nuevaFecha = nuevaFecha.AddDays(+30);
        }
    }
}
